/*
	Boot application
*/	
	var express = require('express');
	var app = express();
	app.enable('trust proxy');
	app.use(express.static(__dirname + '/files'));

	var server = require('http').Server(app);
	server.listen(80);

