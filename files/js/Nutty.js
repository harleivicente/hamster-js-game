/*
    The nutty game

    @param int width
    @param int height

*/
function Nutty(width, height){

    // World & Screen
    this.screen_width =  width;
    this.screen_height = height;
    this.world_height = 1000;
    this.world_width = 3000;

    this.player = null;
    this.text = null;
    this.rage_text = null;

    // Bullies
    this.bullies = [];

    // Instance of Phaser.Game
    this.game = new Phaser.Game(
        this.screen_width,
        this.screen_height,
        Phaser.AUTO,
        '', 
        {   
            self: this,
            preload: this.preload,
            create: this.create,
            update: this.update
        }
    );

}

Nutty.prototype.preload = function(){
    this.self.game.load.image('sky', 'assets/sky.png');
    this.self.game.load.image('bully', 'assets/troll.png');
    this.self.game.load.image('star', 'assets/star.png');
    this.self.game.load.image('ground', 'assets/ground.png');
    this.self.game.load.image('plataform', 'assets/platform.png');
    this.self.game.load.spritesheet('dude', 'assets/nutty.png', 50, 50);
}

Nutty.prototype.create = function(){
    var instance = this;

    //  Enable the Arcade Physics system
    this.self.game.physics.startSystem(Phaser.Physics.ARCADE);

    /*
        World
    */
        this.self.game.add.tileSprite(0, 0, this.self.world_width, this.self.world_height, 'sky');
        this.self.game.world.setBounds(0, 0, this.self.world_width, this.self.world_height);
    
    /*
        Heads-up text
    */
    this.self.text = this.game.add.text(16, 16, '+ + +', { fontSize: '32px', fill: '#f00' });
    this.self.text.fixedToCamera = true;
    this.self.rage_text = this.game.add.text(16, 48, '', { fontSize: '32px', fill: '#f00' });
    this.self.rage_text.fixedToCamera = true;

    /*
        Player
    */
        this.self.player = new Hamster(this.self, 50, this.self.world_height - 200);

    /*
        Bullies
    */
        this.self.bullies.push(new Bully(this.self, 300, 550, 380));
        this.self.bullies.push(new Bully(this.self, 300, 50, 380));
        this.self.bullies.push(new Bully(this.self, 1200, 800, 800));
        this.self.bullies.push(new Bully(this.self, 2500, 400, 600));
        this.self.bullies.push(new Bully(this.self, 800, 280, 380));

    /*
        Plataforms and Sky
    */
        this.self.platform_group = this.self.game.add.group();
        this.self.platform_group.enableBody = true;
        this.self.ground = this.self.platform_group.create(0, this.self.game.world.height - 64, 'ground');
        this.self.ground.body.immovable = true;

        var plataforms_config = [
            {x: 300, y: 700},
            {x: 800, y: 500},
            {x: 1200, y: 200},
            {x: 1800, y: 100},
            {x: 1800, y: 900},
            {x: 1800, y: 1000},
            {x: 2000, y: 1000},
            {x: 300, y: 300}
        ];

        plataforms_config.forEach(function(one){
            instance.self.platform_group.create(one.x, one.y, 'plataform').body.immovable = true;
        });

    /*
        Rocks    
    */
        this.self.rock_group = this.self.game.add.group();
        this.self.rock_group.enableBody = true;


    /*
        Controls
    */
        this.self.dash_key = this.game.input.keyboard.addKey(Phaser.Keyboard.R);
        this.self.attack_key = this.game.input.keyboard.addKey(Phaser.Keyboard.E);
        this.self.cursors = this.self.game.input.keyboard.createCursorKeys();
}


Nutty.prototype.update = function(){
    var idle = true;

    // Bullies
    this.self.bullies.forEach(function(bully){
        bully.update();
    });

    // Rock and plataform
    this.self.game.physics.arcade.collide(this.self.rock_group, this.self.platform_group);

    // Player
    this.self.player.update();

}
