/*
	Hamster

	@param nutty Phaser.Game
*/
function Hamster(nutty, initial_x, initial_y){
	this.nutty = nutty;
	this.initial_x = initial_x;
	this.initial_y = initial_y;
    this.lifes = 3;
    this.last_jump = 0;
    this.face_left = false;
    this.double_jump = true;
    this.rage = 5;

	// Stats
	this.attack_speed = 2;
	this.base_movement_speed = 300;
	this.last_attack_time = 0;
	this.rage_burn_time = 0;

    this.sprite = this.nutty.game.add.sprite(
    	this.initial_x,
    	initial_y,
    	'dude'
	);

    this.nutty.game.physics.arcade.enable(this.sprite);
    this.sprite.body.bounce.y = 0.1;
    this.sprite.body.gravity.y = 800;
    this.sprite.body.collideWorldBounds = false;

    // Camera follow
    this.nutty.game.camera.follow(this.sprite);

    // Animation
    this.sprite.animations.add('left', [3, 4, 5, 6, 7], 10, true);
    this.sprite.animations.add('right', [3, 4, 5, 6, 7], 10, true);
    this.sprite.animations.add('idle', [0, 1, 2], 10, true);
    this.sprite.animations.add('jump', [9, 9, 9, 0, 0, 0, 8, 8], 30, true);
    this.sprite.animations.add('dash', [10, 9, 10], 10, true);
}

Hamster.prototype.update_rage_text = function(){
	this.nutty.rage_text.text = this.rage;
}

Hamster.prototype.consume_rage = function(){
	now = new Date;
	if(now.getTime() - this.rage_burn_time > 50 && this.dash_on){
		now = new Date;
		this.rage_burn_time = now.getTime();
		this.rage = Math.round(this.rage - 1);
		if(this.rage === 1){
			this.rage = 0;
		}
	}

}

Hamster.prototype.update = function(){
    var idle = true;

	// Collide player and plataform
    this.nutty.game.physics.arcade.collide(this.sprite, this.nutty.platform_group);

    // Kill player if out of map 
    if(this.sprite.body.position.y > this.nutty.world_height){
       this.kill();
    } else if(this.sprite.body.position.x >= 3000) {
    	this.kill();
    }



    // Consume rage
    this.consume_rage();
    
    // Reset double jump flag
    if(this.sprite.body.touching.down) {
        this.double_jump = false;
    }

    // Jump
    if(this.nutty.cursors.up.isDown) {
  		var now = new Date;
				
        if(!this.double_jump && (now.getTime() - this.last_jump > 200) && !this.sprite.body.touching.down){
        	idle = false;
  			now = new Date;
			this.last_jump = now.getTime();
	        this.sprite.animations.play('jump');
    	    this.sprite.body.velocity.y = -480;
    	    this.double_jump = true;
        } else if(this.sprite.body.touching.down){
        	idle = false;
			now = new Date;
			this.last_jump = now.getTime();
	        this.sprite.animations.play('jump');
    	    this.sprite.body.velocity.y = -480;
        }
    }

    // Dash
    if(this.nutty.dash_key.isDown && this.rage > 1){
    	idle = false;
    	this.dash_on = true;
        this.sprite.animations.play('dash');
    	this.sprite.body.velocity.x = (this.face_left) ? -1000 : 1000;
    } else {
    	this.dash_on = false;

	    // Left
	    if (this.nutty.cursors.left.isDown) {
	        idle = false;
	        this.face_left = true;
	        if(this.sprite.body.touching.down)
	            this.sprite.body.velocity.x = -300;
	        else
	            this.sprite.body.velocity.x = -200;

	        this.sprite.animations.play('left');
	        
	    }
	    
	    //  Right
	    if (this.nutty.cursors.right.isDown) {
	        this.face_left = false;
	        idle = false;
	        if(this.sprite.body.touching.down)
	            this.sprite.body.velocity.x = 300;
	        else
	            this.sprite.body.velocity.x = 200;

	        this.sprite.animations.play('right');

	    } 
    	
    }

    // Attack (cant attack if dashing
    if(this.nutty.attack_key.isDown && !this.dash_on){
    	this.attack();
    }


    // Idle
    if(idle) {
    	var current = this.sprite.body.velocity.x;
        this.sprite.body.velocity.x = current * .9;
        this.sprite.animations.play('idle');
    }

    // Rage update
    this.update_rage_text();

}

Hamster.prototype.damage = function(enemy){
	if(this.dash_on){
		enemy.kill(this);
	} else {
	    if(this.lifes === 1){
	        this.kill();
	    } else {
	        this.lifes--;
	        var new_text = "";
	        for (var i = this.lifes; i > 0; i--) {
	            new_text += "+ ";
	        };
	        this.nutty.text.text = new_text;
	    }
	}
}

Hamster.prototype.kill = function(){
        this.sprite.body.position.x = 100;
        this.face_left = false;
        this.sprite.body.position.y = 800;
        this.lifes = 3;
        this.double_jump = true;
        this.rage = 0;
        this.nutty.text.text = "+ + +";
}

Hamster.prototype.attack = function(){
	var now = new Date;
	var now_time = now.getTime();

	if((now_time - this.last_attack_time) > ((1 / this.attack_speed) * 1000)){
		this.last_attack_time = now.getTime();

	    var rock = this.nutty.rock_group.create(
    		this.sprite.body.position.x,
    		this.sprite.body.position.y + 20,
    		'star'
		);
    	this.nutty.game.physics.arcade.enable(rock);
    	rock.lifespan = 1000;
		rock.body.velocity.x = this.sprite.body.velocity.x*.4 + (this.face_left ? -500 : 500);
		rock.body.velocity.y = (this.sprite.body.velocity.y*.2 - 150);
		rock.body.gravity.y = 800;
		rock.body.drag.x = 200;

	}
}

