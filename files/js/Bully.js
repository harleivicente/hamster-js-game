/*
	Bully

	@param instance Phaser.Game
*/
function Bully(instance, initial_x, initial_y, range){
	this.nutty = instance;
	this.initial_x = initial_x;
	this.initial_y = initial_y;
	this.range = range;
	this.direction = true;

	// Stats
	this.attack_speed = 0.5;
	this.health = 3;
	this.base_movement_speed = 300;
	this.last_attack_time = 0;

    var random_percent = Math.floor(Math.random() * 100) / 100;

    this.sprite = this.nutty.game.add.sprite(
    	this.initial_x  + this.range * random_percent,
    	initial_y,
    	'bully'
	);

    this.nutty.game.physics.arcade.enable(this.sprite);
    this.sprite.body.gravity.y = 800;
    this.sprite.body.velocity.x = 200;
    this.sprite.body.collideWorldBounds = false;
}

Bully.prototype.update = function(){
	var instance = this;

    this.nutty.game.physics.arcade.collide(this.sprite, this.nutty.platform_group);

	if(this.sprite.body.position.x >= this.initial_x + this.range){
		this.sprite.body.velocity.x = -this.base_movement_speed;
		this.direction = false;
	} else if(this.sprite.body.position.x <= this.initial_x) {
		this.sprite.body.velocity.x = this.base_movement_speed;
		this.direction = true;
	} else if (this.sprite.body.velocity.x === 0){
		if (this.direction)
			this.sprite.body.velocity.x = this.base_movement_speed;
		else
			this.sprite.body.velocity.x = -this.base_movement_speed;
	}

    this.nutty.game.physics.arcade.overlap(this.nutty.player.sprite, this.sprite, function(){
    	instance.attack();
    }, null, this);

    this.nutty.game.physics.arcade.overlap(this.nutty.rock_group, this.sprite, function(bully, rock){
    	rock.destroy();
    	instance.take_dmg();
    }, null, this);

}

Bully.prototype.attack = function(){
	var now = new Date;
	var now_time = now.getTime();

	this.sprite.body.velocity.x = 0;

	if((now_time - this.last_attack_time) > ((1 / this.attack_speed) * 1000)){
		this.last_attack_time = now.getTime();	
		this.sprite.body.velocity.y = -100;
		this.nutty.player.damage(this);
	}
}

Bully.prototype.kill = function(enemy){
	enemy.rage += 3;
	this.sprite.destroy();
}

Bully.prototype.take_dmg = function(){
	if(this.health === 1){
		this.kill(this.nutty.player);
	} else {
		this.health--;
		this.sprite.body.velocity.y = -100;
	}
}

